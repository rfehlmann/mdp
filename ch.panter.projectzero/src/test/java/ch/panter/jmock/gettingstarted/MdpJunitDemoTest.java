package ch.panter.jmock.gettingstarted;

import junit.framework.Assert;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.util.Locale;

/**
 * Created with IntelliJ IDEA.
 * User: raphael
 * Date: 30.09.13
 * Time: 17:45
 * To change this template use File | Settings | File Templates.
 */
public class MdpJunitDemoTest {

    IMdpJunitDemo instance = null;

    @Before
    public void setUp() throws Exception {
        instance = new MdpJunitDemo();
    }

    @After
    public void tearDown() throws Exception {
        instance = null;
    }

    @Test
    public void testIsEmptyEmptyParameter() throws Exception {
        String testParameter = "";
        boolean result = this.instance.isEmpty(testParameter);
        Assert.assertEquals(result, true);
    }

    @Test
    public void testIsEmptyNullParameter() throws Exception {
        String testParameter = null;
        boolean result = this.instance.isEmpty(testParameter);
        Assert.assertEquals(result, true);
    }

    @Test
    public void testIsEmptyNotEmptyParameter() throws Exception {
        String testParameter = "hsuhdfhudsifh";
        boolean result = this.instance.isEmpty(testParameter);
        Assert.assertEquals(result, false);
    }

    @Test
    public void testCapitalizeAllLowCase() throws Exception {
        String testParmeter = "hello";
        String result = this.instance.capitalize(testParmeter);
        Assert.assertEquals("Hello", result);

    }

    @Test
    public void testCapitalizeAllUpperCase() throws Exception {
        String testParmeter = "HELLO";
        String result = this.instance.capitalize(testParmeter);
        Assert.assertEquals("Hello", result);

    }

    @Test
    public void testCapitalizeNumbersAndLetters() throws Exception {
        String testParmeter = "123ABC";
        String result = this.instance.capitalize(testParmeter);
        Assert.assertEquals("123abc", result);

    }

    @Test(expected = NullPointerException.class)
    public void testReverse() throws Exception {
        String testParameter = null;
        String result = this.instance.reverse(testParameter);

    }

    @Test
    public void testReverseString() throws Exception {
        String testParameter = "hsz-t";
        String result = this.instance.reverse(testParameter);
        Assert.assertEquals(result, "t-zsh");
    }

    @Test
    public void testJoin() throws Exception {
        String result = this.instance.join("a", "b", "c");
        Assert.assertEquals(result, "a b c");
    }
}
