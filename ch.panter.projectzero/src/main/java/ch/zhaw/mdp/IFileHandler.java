package ch.zhaw.mdp;

/**
 * Created with IntelliJ IDEA.
 * User: raphael
 * Date: 03.10.13
 * Time: 20:28
 * To change this template use File | Settings | File Templates.
 */
public interface IFileHandler {

    /**
     *
     * @param path
     * @return
     */
    public String getFileContent(String path);

    /**
     *
     * @param content
     * @param path
     */
    public void writeContentToFile(StringBuffer content, String path);

}
