package ch.zhaw.mdp;

import org.junit.Test;

import java.util.Iterator;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * Created with IntelliJ IDEA.
 * User: raphael
 * Date: 03.10.13
 * Time: 20:35
 * To change this template use File | Settings | File Templates.
 */
public class FileHandlerTest {

    @Test
    public void testFileHandler(){
        //arrange
        IFileHandler instance =mock(IFileHandler.class);
        when(instance.getFileContent("test")).thenReturn("hallo Welt");
        String res = instance.getFileContent("test");
        assertEquals(res,"hallo Welt");
        //act

    }



}
