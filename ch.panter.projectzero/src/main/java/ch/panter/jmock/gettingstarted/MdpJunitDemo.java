package ch.panter.jmock.gettingstarted;


/**
 * Created with IntelliJ IDEA.
 * User: raphael
 * Date: 30.09.13
 * Time: 17:41
 * To change this template use File | Settings | File Templates.
 */
public class MdpJunitDemo implements  IMdpJunitDemo{


    @Override
    public boolean isEmpty(String s) {

        return s == null || s.length() == 0;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public String capitalize(String s) {
        String upper = s.substring(0,1);
        upper = upper.toUpperCase();
        String lower = s.substring(1,s.length()).toLowerCase();
        return upper += lower; //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public String reverse(String s) throws NullPointerException {
        if(s == null){
            throw  new NullPointerException();
        }
        String result = "";
        for(int n = s.length()-1;n>=0;n--){
            result +=s.charAt(n);
        }
        return result;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public String join(String... strings) {
        String result = "";
        boolean first = true;
        for(String string:strings){
            if(!first){
                result += " ";
            }
           result += string;
            first = false;
        }

        return result;  //To change body of implemented methods use File | Settings | File Templates.
    }
}
